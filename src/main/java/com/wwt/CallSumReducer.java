package com.wwt;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class CallSumReducer  extends Reducer<Text, CallBean,Text,CallBean> {
    protected void reduce(Text key,Iterable<CallBean> values,Context context) throws IOException, InterruptedException {
        //打入和打出计算
        long inCall=0;
        long outCall=0;
        //打入和打出计算
        for (CallBean bean:values){
            inCall+=bean.getInCall();
            outCall+=bean.getOutCall();
        }

        //输出结果
        context.write(key,new CallBean(key.toString(),inCall,outCall));
    }
}
