package com.wwt;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class CallSumMapper extends Mapper<LongWritable, Text,Text,CallBean> {
    protected void map(LongWritable key,Text value,Context context) throws IOException,InterruptedException {
        //拿到一行数据
        String line=value.toString();
        //切分字段
        String[] fields= StringUtils.split(line," ");
        //拿到手机号的字段
        String phone=fields[0];
        //获取打入的次数
        long inCall=Long.parseLong(fields[1]);
        //获取打出的次数
        long outCall= Long.parseLong(fields[2]);

        //封装数据
        context.write(new Text(phone),new CallBean(phone,inCall,outCall));
    }
}
