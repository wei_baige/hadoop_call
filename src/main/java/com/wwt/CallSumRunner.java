package com.wwt;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class CallSumRunner extends Configured implements Tool {
    public int run(String[] strings) throws Exception {
        Configuration conf=new Configuration();
        Job job=Job.getInstance(conf);

        job.setJarByClass(CallSumRunner.class);
        job.setMapperClass(CallSumMapper.class);
        job.setReducerClass(CallSumReducer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(CallBean.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(CallBean.class);

        FileInputFormat.setInputPaths(job,new Path(strings[0]));
        FileOutputFormat.setOutputPath(job,new Path(strings[1]));
        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        int res= ToolRunner.run(new Configuration(),new CallSumRunner(),args);
        System.exit(res);
    }
}
