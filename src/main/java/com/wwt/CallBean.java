package com.wwt;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class CallBean  implements Writable {
    //号码
    private String phone;
    //接听
    private long inCall;
    //打出
    private long outCall;
    //通话总和
    private long callSum;

    public CallBean(String phone, long inCall, long outCall) {
        this.phone = phone;
        this.inCall = inCall;
        this.outCall = outCall;
        this.callSum = inCall+outCall;
    }

    public CallBean() {

    }

    @Override
    public String toString() {
        return ""+inCall+"\t"+outCall+"\t"+callSum;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getInCall() {
        return inCall;
    }

    public void setInCall(long inCall) {
        this.inCall = inCall;
    }

    public long getOutCall() {
        return outCall;
    }

    public void setOutCall(long outCall) {
        this.outCall = outCall;
    }

    public long getCallSum() {
        return callSum;
    }

    public void setCallSum(long callSum) {
        this.callSum = callSum;
    }

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(phone);
        dataOutput.writeLong(inCall);
        dataOutput.writeLong(outCall);
        dataOutput.writeLong(callSum);
    }

    public void readFields(DataInput dataInput) throws IOException {
        phone=dataInput.readUTF();
        inCall=dataInput.readLong();
        outCall=dataInput.readLong();
        callSum=dataInput.readLong();
    }
}
