# Hadoop序列化

#### 介绍
通过hadoop序列化自定义手机通话次数的统计


#### 软件架构
软件架构说明


#### 安装教程

1.  将项目生成jar包（idea可通过maven插件指令生成 mvn clean package）
2.  将jar包和tong.log数据远程输送到linux环境下的Hadoop里

#### 使用说明

1. 创建Hadoop数据存放点将tong.log放进去，通过自定义jar包扫描

#### 执行效果

![执行jar包](https://images.gitee.com/uploads/images/2020/0519/153010_346b2f67_5715970.png "执行jar包.png")

![查看执行结果](https://images.gitee.com/uploads/images/2020/0519/153034_e745b21f_5715970.png "查看执行结果.png")